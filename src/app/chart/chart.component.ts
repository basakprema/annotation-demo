import { Component, OnInit, ViewChild } from "@angular/core";
import "anychart";
@Component({
  selector: "app-chart",
  templateUrl: "./chart.component.html",
  styleUrls: ["./chart.component.scss"]
})
export class ChartComponent implements OnInit {
  @ViewChild("container") container;
  @ViewChild("boxcontainer") boxcontainer;
  @ViewChild("polarcontainer") polarcontainer;
  @ViewChild("linecontainer") linecontainer;
  @ViewChild("circularcontainer") circularcontainer;
  @ViewChild("singlelinecontainer") singlelinecontainer;
  @ViewChild("columncontainer") columncontainer;

  // legend = anychart.standalones.legend();
  // stage = anychart.graphics.create("polarcontainer");
  chart: anychart.charts.Pie = null;
  boxchart = anychart.box();
  polarchart = anychart.polar();
  linechart = anychart.area();
  circularchart = anychart.gauges.circular();
  singlelinechart = anychart.area();
  columnchart = anychart.column();
  circulardata = [23, 34, 67, 93, 56, 100];
  dataSet = anychart.data.set(this.circulardata);
  palette = anychart.palettes
    .distinctColors()
    .items([
      "#64b5f6",
      "#1976d2",
      "#ef6c00",
      "#ffd54f",
      "#455a64",
      "#96a6a6",
      "#dd2c00",
      "#00838f",
      "#00bfa5",
      "#ffa000"
    ]);
  boxdata = [
    { x: "Jan", low: 0, q1: 1000, median: 1200, q3: 2000, high: 0 ,outliers: [3000]},
    { x: "Feb", low: 0, q1: 2500, median: 3800, q3: 4000, high: 0,outliers: [3000] },
    { x: "Mar", low: 0, q1: 2000, median: 2500, q3: 3000, high: 0 ,outliers: [3000]},
    { x: "Apr", low: 0, q1: 4000, median: 6500, q3: 6900, high: 0,outliers: [3000] },
    { x: "May", low: 0, q1: 8000, median: 8500, q3: 9000, high: 0,outliers: [3000] }
  ];
  boxdata2 = [
    { x: "Jan", low: 4000, q1: 4000, median: 6200, q3: 7000, high: 7000,outliers: [6500] },
    { x: "Feb", low: 1500, q1: 1500, median: 2500, q3: 3500, high: 3500,outliers: [6500] },
    { x: "Mar", low: 2000, q1: 2000, median: 3000, q3: 3000, high: 3000 ,outliers: [6500]},
    { x: "Apr", low: 3600, q1: 3600, median: 6900, q3: 8000, high: 8000 ,outliers: [6500]},
    { x: "May", low: 7800, q1: 7800, median: 8350, q3: 9000, high: 9000 ,outliers: [6500]}
  ];
  polardata = [{ x: "A", value: 4 }];
  polardata2 = [{ x: "D", value: 50 }];
  polardata3 = [{ x: "C", value: 30 }];
  

  linedata1 = [
    { x: "January", value: 65,marker:{enabled:true} },
    { x: "February", value: 59 ,marker:{enabled:true}},
    { x: "March", value: 80 ,marker:{enabled:true}},
    { x: "April", value: 81,marker:{enabled:true} },
    { x: "May", value: 56 ,marker:{enabled:true}},
    { x: "June", value: 55,marker:{enabled:true} },
    { x: "July", value: 40,marker:{enabled:true}},
  ];
  linedata2 = [
    { x: "January", value: 28 ,marker:{enabled:true}},
    { x: "February", value: 48,marker:{enabled:true} },
    { x: "March", value: 40,marker:{enabled:true} },
    { x: "April", value: 19 ,marker:{enabled:true}},
    { x: "May", value: 86,marker:{enabled:true}},
    { x: "June", value: 27 ,marker:{enabled:true}},
    { x: "July", value: 90,marker:{enabled:true}},
  ];
  singlelinedata1 = [
    { x: "January", value: 65,marker:{enabled:true} },
    { x: "February", value: 59 ,marker:{enabled:true}},
    { x: "March", value: 80 ,marker:{enabled:true}},
    { x: "April", value: 81,marker:{enabled:true} },
    { x: "May", value: 56 ,marker:{enabled:true}},
    { x: "June", value: 55,marker:{enabled:true} },
    { x: "July", value: 40,marker:{enabled:true}},
  ];
  singlelinedata2 = [
    { x: "January", value: 28 ,marker:{enabled:true}},
    { x: "February", value: 48,marker:{enabled:true} },
    { x: "March", value: 40,marker:{enabled:true} },
    { x: "April", value: 19 ,marker:{enabled:true}},
    { x: "May", value: 86,marker:{enabled:true}},
    { x: "June", value: 27 ,marker:{enabled:true}},
    { x: "July", value: 90,marker:{enabled:true}},
  ];
  columndata = anychart.data.set([
    ['Lip gloss', 22998, 12043],
    ['Eyeliner', 12321, 15067],
    ['Eyeshadows', 12998, 12043],
    ['Powder', 10261, 14419],
    ['Mascara', 11261, 10419]
]);

  // boxseries2 = this.boxchart.box(this.boxdata2);
  polarseries = this.polarchart.column(this.polardata);
  polarseries3 = this.polarchart.column(this.polardata3);
  polarseries2 = this.polarchart.column(this.polardata2);
  lineseries1 = this.linechart.splineArea(this.linedata1);
  lineseries2 = this.linechart.splineArea(this.linedata2);
  singlelineseries1 = this.singlelinechart.line(this.singlelinedata1)
  singlelineseries2 = this.singlelinechart.line(this.singlelinedata2)
  columnseriesdata= this.columndata.mapAs({'x':0,'value':1});
  columnseriesdata2= this.columndata.mapAs({'x':0,'value':2})
  columnseries = this.columnchart.column(this.columnseriesdata);
  // columnseries2 = this.columnchart.column(this.columnseriesdata2);
  boxseries = this.boxchart.box(this.boxdata);
  constructor() {}
  private data_: Array<Object> = [
    {
      customName: "Name1",
      customValue1: 10,
      customValue2: 12,
      customValue3: 7
    },
    {
      customName: "Name2",
      customValue1: 14,
      customValue2: 10,
      customValue3: 17
    },
    { customName: "Name3", customValue1: 21, customValue2: 4, customValue3: 15 }
  ];

  private dataSet_: anychart.data.Set = anychart.data.set(this.data_);

  private mappings_: { [key: string]: anychart.data.View } = {
    data1: this.dataSet_.mapAs({ x: ["customName"], value: ["customValue1"] }),
    data2: this.dataSet_.mapAs({ x: ["customName"], value: ["customValue2"] }),
    data3: this.dataSet_.mapAs({ x: ["customName"], value: ["customValue3"] })
  };
  public getDataList() {
    let res: Array<string> = [];
    for (let key in this.mappings_) {
      res.push(key);
    }
    return res;
  }

  public getData(key: string = "data1") {
    return this.mappings_[key];
  }

  makeBarWithBar = function(gauge, radius, i, width, without_stroke) {
    var stroke = "1 #e5e4e4";
    if (without_stroke) {
      stroke = null;
      gauge
        .label(i)
        // .text(names[i] + ', <span style="">' + data[i] + '%</span>')// color: #7c868e
        .useHtml(true);
      gauge
        .label(i)
        .hAlign("center")
        .vAlign("middle")
        .anchor("right-center")
        .padding(0, 10)
        .height(width / 2 + "%")
        .offsetY(radius + "%")
        .offsetX(0);
    }

    gauge
      .bar(i)
      .dataIndex(i)
      .radius(radius)
      .width(width)
      .fill(this.palette.itemAt(i))
      .stroke(null)
      .zIndex(5);
    gauge
      .bar(i + 100)
      .dataIndex(5)
      .radius(radius)
      .width(width)
      .fill("#F5F4F4")
      .stroke(stroke)
      .zIndex(4);
      
    // return gauge.bar(i);
  };
  shapes = this.columnseries.rendering().shapes();
  oncompanyselection(opt){
    this.boxchart.removeAllSeries()

    switch(opt){
      case 'A':
          this.boxseries = this.boxchart.box(this.boxdata)
    this.boxchartconfiguration()

      break;

      case 'B':
  this.boxseries = this.boxchart.box(this.boxdata2);
  this.boxchartconfiguration()

        break;
    }
  }
  boxchartconfiguration(){
    this.boxchart.draw();
    this.boxchart.container(this.boxcontainer.nativeElement);
    this.boxseries.normal().stemStroke("transparent");
    // this.boxseries2.normal().stemStroke("transparent");
    this.boxseries.normal().stroke("#b65bf1");
    // this.boxseries2.normal().stroke("#f5c983");
    this.boxseries.normal().medianStroke('black')
    // this.boxseries2.normal().medianStroke('black')
    // this.boxseries.normal().outlierMarkers()
    this.boxchart.legend(true);

    this.boxseries.fill({
      keys: [".0.25 #b65bf1 ", ".9 #9211e8"],
      angle: -90,
      opacity: 1
    });
  }
  ngAfterViewInit() {
    this.boxchartconfiguration()
    this.chart.container(this.container.nativeElement);
    this.polarchart.container(this.polarcontainer.nativeElement);
    this.linechart.container(this.linecontainer.nativeElement);
    this.circularchart.container(this.circularcontainer.nativeElement);
    this.singlelinechart.container(this.singlelinecontainer.nativeElement);
    this.columnchart.container(this.columncontainer.nativeElement)
    this.chart.draw();
    this.polarchart.draw();
    this.linechart.draw();
    let shapes = this.columnseries.rendering().shapes();;
    this.columnchart.pointWidth(10)
    this.columnseries.rendering()
      // set point function to drawing
      .point(drawer)
      // set update point function to drawing, change the point shape when the state changes
      .updatePoint(drawer)
      // set shapes
      .shapes(shapes);
    // this.columnseries2.rendering()
    // // set point function to drawing
    // .point(drawer)
    // // set update point function to drawing, change the point shape when the state changes
    // .updatePoint(drawer)
    // // set shapes
    // .shapes(shapes);
    this.columnchart.draw();
    this.circularchart.draw();

    //-------------------single line chart ---------------
    this.singlelinechart.draw()
    this.singlelinechart.legend().enabled(true)
    this.singlelineseries1.normal().stroke({
      keys: [".2 #c94509", ".9 #e69d59"],
      // angle: 360,
      thickness:4
    });
    this.singlelineseries2.normal().stroke({
      keys: [".2 #18e81e", ".9 #c1e818"],
      thickness:4,
    });
    //---------------------line----------------------------
    this.linechart.legend().enabled(true)
    this.lineseries1.fill({
      keys: ["0.25 #f0466f ", ".9 #9a0f31"],
      angle: 90,
      opacity: 1
    });
    this.lineseries2.fill({
      keys: ["0.25 #74ddf2 ", ".9 #15ccf2"],
      angle: 90,
      opacity: 1
    });
    // -------------------- box -----------------------------
    
    // this.boxseries2.fill({
    //   keys: [".0.25 #f5c983 ", ".9 #f5a729"],
    //   angle: -90,
    //   opacity: 1
    // });
    // --------------------- circular -----------------------

    this.circularchart.data(this.dataSet);
    this.circularchart
      .fill("#fff")
      .stroke(null)
      .padding(0)
      .margin(100)
      .startAngle(0)
      // .sweepAngle();
    // this.circularchart.label().enabled(false)
    var axis = this.circularchart
      .axis()
      .radius(150)
      .width(1)
      .fill(null);
    axis.scale().minimum(0);
    // .maximum(100)
    // .ticks({interval: 1})
    // .minorTicks({interval: 1});
    axis.labels().enabled(false);
    axis.ticks().enabled(false);
    axis.minorTicks().enabled(false);
    this.makeBarWithBar(this.circularchart, 100, 5, 2,false);
    this.makeBarWithBar(this.circularchart, 80, 1, 2, false);
    this.makeBarWithBar(this.circularchart, 60, 2, 3, false);
    this.makeBarWithBar(this.circularchart, 40, 3, 3, false);
    this.makeBarWithBar(this.circularchart, 20, 4, 7, false);

    this.circularchart.margin(50);
    this.circularchart
      .title()
      .text(
        "Medicine manufacturing progress" +
          '<br/><span style="color:#929292; font-size: 12px;">(ACME CORPORATION)</span>'
      )
      .useHtml(true);
    this.circularchart
      .title()
      .enabled(true)
      // .hAlign("center")
      .padding(0)
      .margin([0, 0, 20, 0]);

    // ----------------------polar chart---------------------------------
    this.polarchart.yAxis().enabled(false);
    this.polarchart.xAxis().enabled(false);
    this.polarchart.xScale("ordinal");
    this.polarseries.normal().stroke('transparent')
    this.polarseries2.normal().stroke('transparent')
    this.polarseries3.normal().stroke('transparent')

    this.polarseries.fill({
      keys: [".3 #cf8011", ".9 yellow"],
      angle: 217,
      opacity: 1
    });
    this.polarseries2.fill({
      keys: ["0.25 #74ddf2", ".9 #15ccf2"],
      angle: 217,
      opacity: 1
    });
    this.polarseries3.fill({
      keys: [".3 #1fe95b", ".9 #1fe9d9"],
      angle: 217,
      opacity: 1
    });
    this.polarchart.sortPointsByX(true);
    // this.polarchart.yScale()["stackMode"]("percent");
    this.polarchart.xGrid().enabled(false);
    this.polarchart.yGrid().enabled(false);
    this.polarchart.innerRadius(50);
    this.polarchart.legend(true);
    this.polarseries.name("polar 1");
    this.polarseries2.name("polar 2");
    this.polarseries3.name("polar 3");
    let x = this.polarseries.color();
    this.polarseries.legendItem().format("{%seriesName}");
    this.polarseries2.legendItem().format("{%seriesName}");
    this.polarseries3.legendItem().format("{%seriesName}");
  }
  ngOnInit() {
    this.chart = anychart.pie(this.getData("data1"));
  }
}

function drawer() {

  // if missing (not correct data), then skipping this point drawing
  if (this.missing) {
      return;
  }
  // get shapes group
  var shapes = this.shapes || this.getShapesGroup(this.pointState);
  // calculate the left value of the x-axis
  var leftX = this.x - 20 / 2;
  // calculate the right value of the x-axis
  var rightX = leftX + 20;
  // calculate the half of point width
  var rx = 20 / 2;

  shapes.path
          // resets all 'line' operations
          .clear()
          // draw column with rounded edges
          .moveTo(leftX, this.zero)
          .lineTo(leftX, this.value + rx)
          .circularArc(leftX + rx, this.value + rx, rx, rx, 180, 180)
          .lineTo(rightX, this.zero)
          .fill({ keys: ["0.25 #74ddf2 ", ".9 #15ccf2"],angle:90})
          // close by connecting the last point with the first straight line
          .close();
}

