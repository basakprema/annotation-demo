import { Component, OnInit, AfterViewInit } from '@angular/core';
// import { fabric } from 'fabric';
import { environment } from './../../environments/environment';
import { from } from 'rxjs';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  context: any;
  previousRect: any;
  selectedDimension:any;
  docImage:any;
  unallocated:boolean;
  selectedLineItem:any;
  annotation = environment.data.body.source.components[0];
  pages = environment.data.body.source.pages
  selectedPage:any;
onOptionSelect(opt){
  this.selectedLineItem = opt
  this.pages.forEach(element => {
    if(opt.pageref == element.pageno){
      this.selectedPage = element
    }
  });
this.selectedDimension = opt.LabelCoordinate
}
deleteDiv(selectedDiv){
  selectedDiv.deleted = true; 
}
selectDiv(selectedDiv){
if(!selectedDiv.show){
  selectedDiv.show = true;
  this.getPosition(selectedDiv)
}
}
getPosition(selectedDimension){
  if(this.selectedLineItem){
    let x = document.getElementById("docImage").offsetWidth;
    let ratio = x/this.pages[this.selectedLineItem.pageref-1].resolution[0]
    
    let styles = {}
      styles = {
        'top': `${selectedDimension.dimensions.y*ratio}px` ,
        'left':  `${selectedDimension.dimensions.x*ratio +12}px` ,
        'height':`${selectedDimension.dimensions.h*ratio}px`,
        'width':`${ selectedDimension.dimensions.w*ratio}px`,
        'background':'red',
        // 'zoom':'20%'
      };
    // if(selectedDimension.show){
    //   styles = {
    //     'top': `${selectedDimension.top}px` ,
    //     'left':  `${selectedDimension.left}px` ,
    //     'height':`${selectedDimension.height}px`,
    //     'width':`${selectedDimension.width}px`,
    //     'background':'red'
    //   };
    // } else if (!selectedDimension.show && this.unallocated){
    //   styles = {
    //     'top': `${selectedDimension.top}px` ,
    //     'left':  `${selectedDimension.left}px` ,
    //     'height':`${selectedDimension.height}px`,
    //     'width':`${selectedDimension.width}px`,
    //     'border':`3px dashed red`,
    //     'cursor':`pointer`
    //   };
    // }
    return styles;
  }
}
  constructor() { }
  // onOptionClick(opt) {
  //   if (this.previousRect) {
  //     this.drawImage()
  //   }
  //   setTimeout (() => {
  //     this.context.fillStyle = "rgba(255, 255, 255, 0.3)";
  //     this.context.fillRect(opt.dimensions[0], opt.dimensions[1], opt.dimensions[2], opt.dimensions[3],0.1);
  //     this.previousRect = opt
  //  }, 100);
  // }
  // drawImage(){
  //   var c: any = document.getElementById("canvas");
  //   this.context = c.getContext("2d");
  //   var base_image = new Image();
  //   base_image.src = 'http://fabricjs.com/assets/pug_small.jpg';
  //   base_image.onload = () => {
  //     this.context.drawImage(base_image, 0, 0, 500, 500);
  //   };
  // }
  getDragPosition(evnt){
    let coord = document.getElementById('docImage') 
  }
  drop(evnt){

  }
  getImageDimension(){
    if(this.selectedLineItem){
      let width = this.pages[this.selectedLineItem.pageref-1].resolution[0];
      let height =this.pages[this.selectedLineItem.pageref-1].resolution[1] 

      // let width = (this.pages[this.selectedLineItem.pageref-1].resolution[0]/document.getElementById("imageContainer").offsetWidth)*1000
      // let height = (this.pages[this.selectedLineItem.pageref-1].resolution[1]/document.getElementById("imageContainer").offsetHeight)*1000
      return {
        // zoom: '100%'
        // width:`${width}px`,
        // height:`${height}px`
        width:`100%`,
        // height:`${height}px`
        };
    }
    else return {color:'black'}
  }
  ngAfterViewInit() {
    this.docImage = document.getElementById('docImage')
  // this.drawImage();
  }
  ngOnInit() { }

}
